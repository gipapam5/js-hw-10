const link = document.createElement('a');
link.setAttribute('href', '#');
link.innerText = 'Learn more';
const footer = document.querySelector('footer');
footer.insertAdjacentElement('beforeend', link);




const select = document.createElement('select');
const main = document.querySelector('main');

select.setAttribute('id', 'rating');
main.insertAdjacentElement('afterbegin', select);

for (let i = 1; i < 5; i++) {
    const option = document.createElement('option')
    option.value = `${i}`;
    option.innerText = `${i} Star`;
    select.append(option);
}

// const option1 = document.createElement('option');
// option1.value = '1';
// option1.innerText = '1 Star';

// const option2 = document.createElement('option');
// option2.value = '2';
// option2.innerText = '2 Star';

// const option3 = document.createElement('option');
// option3.value = '3';
// option3.innerText = '3 Star';

// const option4 = document.createElement('option');
// option4.value = '4';
// option4.innerText = '4 Star';

// const defaultOption = document.createElement('option');
// defaultOption.innerText = 'Choose your rating'
// defaultOption.selectedIndex = '1';

// select.append(defaultOption);
// select.append(option1);
// select.append(option2);
// select.append(option3);
// select.append(option4);

