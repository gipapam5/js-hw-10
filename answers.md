1. Існує декілька способів додати або створити елемент. Що б створити елемент можна використати команду document.createElement(),
   також для цього можна використовувати innerHTML() або insertAdjacentHTML(). Проте останні 2 варіанти дозволяють створити елемент вже всередині якогось. Наприклад
   const container = document.getElementById('container');
   container.innerHTML = '<div>Hello, world!</div>';
   Для того що б добавити DOM елемент є способи такі як:
   append() - дозволяє вставити елемет в тег перед закриваючою частиною,
   prepend() - дозволяє вставити елемет в тег першим пісял відкриваючої частини,
   before() - вставить елемент перед тегом,
   after() - вставить елемент після тегу
   Також можна використовувати insertAdjacentElement() - який приймає 2 аргументи. перший це куди вставити: beforebegin, afterbegin, beforeend, afterend. А другий аргумент елемент який саме потрібно вставити.

2. const nav = document.querySelector('.navigation');
   nav.remove();

3. Що б вставити елемент саме перед або після іншого DOM елементу нам потрібно буде before() або after(). Як теж можна використати і insertAdjacentElement() з аргументами beforebegin або afterend
